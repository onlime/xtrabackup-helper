import logging
from uuid import uuid4
from sys import stdout, stderr
from os import path

RUN_ID = str(uuid4())[:8]
DEFAULT_LOG_LEVEL = logging.INFO

def configure_logging(logdir: str, verbose: bool, debug: bool):
    log_level = logging.DEBUG if debug else DEFAULT_LOG_LEVEL
    log_format = '%(asctime)s %(levelname)s (' + RUN_ID + ') %(message)s'

    # Configure global logging
    logfile = path.join(logdir, 'application.log')
    log_handlers = [logging.FileHandler(filename=logfile)]
    if verbose:
        log_handlers.append(logging.StreamHandler(stdout))
    logging.basicConfig(level=log_level, format=log_format, handlers=log_handlers)
    logger = logging.getLogger(__name__)

    # log messages above WARNING to stderr
    log_handler_stderr = logging.StreamHandler(stderr)
    log_handler_stderr.setLevel(logging.WARNING)
    logger.addHandler(log_handler_stderr)

    return logger
