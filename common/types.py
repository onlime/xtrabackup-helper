import argparse
import re

backup_dir_pattern = re.compile(r"^[0-9]{12}-(base|inc)$") # YYYYMMDDHHMM-base|-inc
prepare_dir_pattern = re.compile(r"^[a-zA-Z0-9-_]{1,32}$")

def restore_dir_type(value: str, pattern: object = backup_dir_pattern):
    if not pattern.match(value):
        raise argparse.ArgumentTypeError
    return value

def prepare_dir_type(value: str, pattern: object = prepare_dir_pattern):
    if not pattern.match(value):
        raise argparse.ArgumentTypeError
    return value
