# Percona XtraBackup Helper

## Requirements

- Percona Server for MySQL 8.0
- Percona XtraBackup 8.0.x
- Python >= 3.5 (only tested on Python 3.9)

## Prerequisites

Install `xtrabackup`:

```bash
# Percona Server for MySQL 8.0 / Debian Linux
$ apt install percona-xtrabackup-80

# Homebrew / macOS
$ brew install percona-xtrabackup
```

If you run `xtrabackup` as MySQL user `root@localhost`, you might need to add the following privilege:

```sql
mysql> GRANT BACKUP_ADMIN ON *.* TO root@localhost;
```

## Install

Install this project (no need to setup any venv as we don't use any external dependencies):

```bash
$ git clone git@gitlab.com:onlime/xtrabackup-helper.git
$ cd xtrabackup-helper
$ ./xtrabackup-helper.py --help
```

## Usage

### Command help

`xtrabackup-helper.py` is pretty powerful, check `xtrabackup-helper.py --help` for help:

```
usage: xtrabackup-helper.py [-h] [--inc] [--keep KEEP] [--restore-dir RESTORE_DIR] [--prepare-dir PREPARE_DIR]
                            [--prepare-only] [--copy-back] [--datadir DATADIR] [--backup-path BACKUP_PATH]
                            [--log-dir LOG_DIR] [--dryrun] [--verbose] [--debug]
                            {list,backup,restore}

positional arguments:
  {list,backup,restore}
                        Action to be executed

optional arguments:
  -h, --help            show this help message and exit
  --inc, --incremental  Runs incremental backup on latest full backup
  --keep KEEP           Number of full backups to keep (Default: 0 / unlimited)
  --restore-dir RESTORE_DIR
                        Incremental or full backup directory to restore (Default: latest incremental dir)
  --prepare-dir PREPARE_DIR
                        Directory where the restore should be prepared
  --prepare-only        Only prepare the backup but don't restore it yet
  --copy-back           Use --copy-back instead of default --move-back for backup restore
  --datadir DATADIR     The directory in which the database server stores its databases (Default: /var/lib/mysql)
  --backup-path BACKUP_PATH
                        Backup directory (Default: /backup/xtrabackup)
  --log-dir LOG_DIR     Relative logs path (Default: logs)
  --dryrun, --dry-run   Don't do anything, just report commands
  --verbose             Also print all log entries
  --debug               Enable debug mode
```

### Examples

Usage examples:

> **WARNING:** Do not try this at home! We have added `--dryrun` to every command, so that it won't change anything on your existing data. Once you are sure what the command does, run it without the `--dryrun` flag.

```bash
$ cd <PROJECT_DIR>

# Run a full backup
$ ./xtrabackup-helper.py backup --dryrun

# Run an incremental backup (based on last full backup)
$ ./xtrabackup-helper.py backup --inc --dryrun

# Run a full backup and output log lines to STDOUT
$ ./xtrabackup-helper.py backup --verbose --dryrun

# Run a full backup in another backup path (default is /backup/xtrabackup)
$ ./xtrabackup-helper.py backup --backup-path=/tmp/xtrabackup --dryrun

# List all available backups
$ ./xtrabackup-helper.py list

# Restore from latest full/incremental backup
$ ./xtrabackup-helper.py restore --dryrun

# Restore from a specific full/incremental backup
$ ./xtrabackup-helper.py restore --restore-dir=202101011805-inc --dryrun

# Restore from a latest full/incremental backup, but use --copy-back instead of --move-back (default)
$ ./xtrabackup-helper.py restore --copy-back --dryrun

# Prepare latest full/incremental backup, without restoring it yet
$ ./xtrabackup-helper.py restore --prepare-only --dryrun
```

If you did not specify `--verbose`, you might want to monitor the main application log:

```bash
$ tail -f logs/application.log
```

### Cronjob

Example cronjobs for nightly full backups and incremental backups every 2hrs, keeping the last 3 days of backup rotation:

```bash
00 1    * * *	root	cd PROJECT_DIR && && ./xtrabackup-helper.py backup --keep=3
15 */2  * * *	root	cd PROJECT_DIR && && ./xtrabackup-helper.py backup --inc
```

Remember to always first run a full backup. So if you set up above cronjobs, you might want to run initial full backup manually:

```bash
$ ./xtrabackup-helper.py backup
```

## About xtrabackup-helper

The project is hosted on [GitLab](https://gitlab.com/onlime/xtrabackup-helper).
Please use the project's [issue tracker](https://gitlab.com/onlime/xtrabackup-helper/issues) if you find bugs.

xtrabackup-helper was written by [Philip Iezzi](https://gitlab.com/piezzi) for [Onlime GmbH](https://www.onlime.ch).
