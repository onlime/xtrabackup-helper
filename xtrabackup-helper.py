#!/usr/bin/env python3
import argparse
import sys
from os import path, listdir
from shutil import rmtree
from subprocess import check_call
from datetime import datetime
from helper.setup_logging import configure_logging, RUN_ID
from common.types import *

class XtrabackupHelper(object):
    def __init__(self, logger: object, backup_path: str, log_dir: str, dryrun: bool = False):
        self.logger = logger
        self.backup_path = backup_path
        self.log_dir = log_dir
        self.dryrun = dryrun
    
    def get_backup_dirs(self, by_related: str = None):
        dirs = [f for f in listdir(self.backup_path) if backup_dir_pattern.search(f)]
        dirs.sort(reverse=True)
        # only return related backup dirs (from provided by_related dir until last '*-base' dir)
        if by_related:
            related_dirs = []
            for backup_dir in dirs:
                if related_dirs or backup_dir == by_related:
                    related_dirs.append(backup_dir)
                    if backup_dir.endswith('-base'):
                        break
            return related_dirs
        return dirs
    
    def get_latest_base_dir(self):
        for backup_dir in self.get_backup_dirs():
            if backup_dir.endswith('-base'):
                return backup_dir
        return None

    def run_cmd(self, cmd: object, logfile: str = None):
        log_cmd = ' '.join(cmd)
        if logfile:
            logpath = path.join(self.log_dir, logfile)
            log_cmd += ' > ' + logpath
        self.logger.info(log_cmd)
        if not self.dryrun:
            if logfile:
                f = open(logpath, 'a')
                check_call(cmd, stdout=f, stderr=f)
                f.close()
            else:
                check_call(cmd)

    def get_logfile(self, suffix: str = None):
        ts = datetime.now().strftime('%Y%m%d%H%M%S')
        return '{}.log'.format('-'.join(filter(None, [ts, RUN_ID, suffix])))

    def xtrabackup_backup(self, target_dir: str, incremental_basedir: str = None, logfile: str = None):
        # Backup to .part target dir to avoid broken partial backups if script execution gets interrupted
        cmd = [ 'xtrabackup', '--backup', '--target-dir={}'.format(target_dir + '.part') ]
        if incremental_basedir:
            cmd.append('--incremental-basedir={}'.format(incremental_basedir))
        self.run_cmd(cmd, logfile)
        # Not using shutil.move() here as it does not preserve metadata (ownership)
        self.run_cmd(['/bin/mv', target_dir + '.part', target_dir])

    def xtrabackup_prepare(self, target_dir: str, incremental_dir: str = None, apply_log_only: bool = True, logfile: str = None):
        cmd = [ 'xtrabackup', '--prepare', '--target-dir={}'.format(target_dir) ]
        if incremental_dir:
            cmd.append('--incremental-dir={}'.format(incremental_dir))
        if apply_log_only:
            cmd.append('--apply-log-only')
        self.run_cmd(cmd, logfile)
    
    def xtrabackup_restore(self, target_dir: str, datadir: str = None, copy_back: bool = False, logfile: str = None):
        cmd = [ 'xtrabackup', '--target-dir={}'.format(target_dir) ]
        if datadir:
            cmd.append('--datadir={}'.format(datadir))
        cmd.append('--copy-back' if copy_back else '--move-back')
        self.run_cmd(cmd, logfile)

    # Action: list
    def list_backups(self):
        for backup_dir in self.get_backup_dirs():
            print(backup_dir)
    
    # Action: backup
    def backup(self, inc: bool = False, keep: int = 0):
        ts = datetime.now().strftime('%Y%m%d%H%M')
        backup_dir = ts + '-' + ('inc' if inc else 'base')
        target_dir = path.join(self.backup_path, backup_dir)
        if path.exists(target_dir):
            raise FileExistsError(
                'Target backup path {} already exists! Please avoid running multiple backups at the same minute.'.format(target_dir)
            )
        logfile = self.get_logfile('backup-' + backup_dir) # e.g. YYYYMMDDHHMMSS-RUNID-backup-YYYYMMDDHHMM-base.log
        if not inc:
            # Create full backup (*-base)
            self.xtrabackup_backup(target_dir, logfile=logfile)
        else:
            # Create incremental backup (*-inc)
            latest_backup_base = self.get_latest_base_dir()
            if not latest_backup_base:
                raise FileNotFoundError('Could not find any base backup directory ({}/*-base)'.format(self.backup_path))
            incremental_basedir = path.join(self.backup_path, latest_backup_base)
            self.xtrabackup_backup(target_dir, incremental_basedir, logfile=logfile)
        # Backup rotation: removing old backup directories
        if keep > 0:
            count_full = 0
            for backup_dir in self.get_backup_dirs():
                if count_full >= keep:
                    self.logger.info('Removing backup {}: {}'.format(count_full, backup_dir))
                    if not self.dryrun:
                        rmtree(path.join(self.backup_path, backup_dir))
                if backup_dir.endswith('-base'):
                    count_full += 1

    # Action: restore
    def restore(self, restore_dir: str, prepare_dir: str = 'prepared', datadir: str = None, prepare_only: bool = False, copy_back: bool = False):
        # absolute path to prepare directory
        prepare_path = path.join(self.backup_path, prepare_dir)
        if path.exists(prepare_path):
            raise FileExistsError('Prepare path {} already exists! Delete it or move it away first.'.format(prepare_path))
        if not restore_dir:
            # if no restore_dir was provided, choose latest backup dir
            backup_dirs = self.get_backup_dirs()
            if not backup_dirs:
                raise FileNotFoundError('Could not find any backup directories in ' + self.backup_path)
            restore_dir = backup_dirs[0]
        # absolute path to backup directory to restore from
        restore_path = path.join(self.backup_path, restore_dir)
        if not path.isdir(restore_path):
            raise FileNotFoundError('Could not find backup directory {} to restore from.'.format(restore_path))
        # starting preparation phase
        self.logger.info('Preparing restore from {} ...'.format(restore_path))
        # start with *-base dir and proceed with *-inc dirs
        backup_dirs = sorted(self.get_backup_dirs(restore_dir))
        num_backup_dirs = len(backup_dirs)
        for i,backup_dir in enumerate(backup_dirs, start=1):
            is_not_last = i < num_backup_dirs
            curr_backup_path = path.join(self.backup_path, backup_dir)
            # print(backup_dir)
            logfile = self.get_logfile('prepare-' + backup_dir)
            if backup_dir.endswith('-base'):
                # preparing base backup
                self.run_cmd([ 'cp', '-a', curr_backup_path, prepare_path ])
                self.xtrabackup_prepare(
                    target_dir=prepare_path, 
                    apply_log_only=is_not_last,
                    logfile=logfile
                )
                base_path = curr_backup_path
            else:
                # applying incremental backup
                self.xtrabackup_prepare(
                    target_dir=prepare_path, 
                    incremental_dir=curr_backup_path, 
                    apply_log_only=is_not_last,
                    logfile=logfile
                )
        if not prepare_only:
            # Run restore from prepared backup
            self.run_cmd([ '/etc/init.d/mysql', 'stop' ])
            self.run_cmd([ 'mv', datadir, datadir + '.OLD' ])
            logfile = self.get_logfile('restore-' + prepare_dir)
            self.xtrabackup_restore(prepare_path, datadir, copy_back, logfile)
            self.run_cmd([ '/etc/init.d/mysql', 'start' ])


if __name__ == "__main__":
    # argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('action', choices=['list', 'backup', 'restore'], help='Action to be executed')
    # options for BACKUP action
    parser.add_argument('--inc', '--incremental', action='store_true', help='Runs incremental backup on latest full backup')
    parser.add_argument('--keep', type=int, default=0, help='Number of full backups to keep (Default: 0 / unlimited)')
    # options for RESTORE action
    parser.add_argument('--restore-dir', type=restore_dir_type, help='Incremental or full backup directory to restore (Default: latest incremental dir)')
    parser.add_argument('--prepare-dir', type=prepare_dir_type, default='prepared', help='Directory where the restore should be prepared')
    parser.add_argument('--prepare-only', action='store_true', help="Only prepare the backup but don't restore it yet")
    parser.add_argument('--copy-back', action='store_true', help="Use --copy-back instead of default --move-back for backup restore")
    # general options
    parser.add_argument('--datadir', type=str, default='/var/lib/mysql', help='The directory in which the database server stores its databases (Default: /var/lib/mysql)')
    parser.add_argument('--backup-path', type=str, default='/backup/xtrabackup', help='Backup directory (Default: /backup/xtrabackup)')
    parser.add_argument('--log-dir', type=str, default='logs', help='Relative logs path (Default: logs)')
    parser.add_argument('--dryrun', '--dry-run', action='store_true', help="Don't do anything, just report commands")
    parser.add_argument('--verbose', action='store_true', help='Also print all log entries')
    parser.add_argument('--debug', action='store_true', help='Enable debug mode')
    args = parser.parse_args()

    # Configure logging
    logger = configure_logging(args.log_dir, args.verbose, args.debug)

    # Always log full command with all arguments on first logline
    logger.info(' '.join([path.basename(__file__)] + sys.argv[1:]))

    helper = XtrabackupHelper(logger, args.backup_path, args.log_dir, args.dryrun)
    if args.action == "list":
        helper.list_backups()
    elif args.action == "backup":
        helper.backup(args.inc, args.keep)
    elif args.action == "restore":
        helper.restore(args.restore_dir, args.prepare_dir, args.datadir, args.prepare_only, args.copy_back)
    else:
        raise NotImplementedError

    logger.info('done.')
